export interface Album {
    id: string;
    title?: string;
    type?: string;
}
export interface MediaItem {
    id: string;
    thumbnail: string;
}
declare type FileFunction = (success: (file: FileEntry) => any, error: (e: Error) => any) => any;
export interface FileEntry {
    name: string;
    size: number;
    type: string;
    file: FileFunction;
    toInternalURL: () => string;
}
interface Options {
    /** Maxmimum number of returned items */
    count?: number;
}
/**
 * Loads the most recent items from the Camera Roll
 */
declare function load(options: Options): Promise<MediaItem[]>;
/**
 * Finds in the list of available albums the one pointing to the device camera:
 * - iOS: type is "PHAssetCollectionSubtypeSmartAlbumUserLibrary"
 * - Android: title is "Camera"
 * @param albums List of all available albums
 * @return Album representing the Camera Roll
 */
declare function _findCameraRollAlbum(albums: Album[]): Album;
/**
 * Gets the filepath to the high quality version of the mediaitem
 * @param  {Object} item Media item for which the HQ version should be looked up
 * @return Path to the HQ version of the mediaitem
 */
declare function getHQImageData(item: MediaItem): Promise<string>;
/**
 * Gets a reference to a local file
 * @param filePath Path of the to be loaded file
 * @return reference to a local file
 */
declare function getFile(filePath: string): Promise<FileEntry>;
/**
 * Checks if all required libaries are available to load galley items. Use this
 * check to verify if the app runs in a Cordova environment.
 *
 * @return True if items can be loaded from the gallery
 */
declare function isSupported(): boolean;
export { load, getHQImageData, getFile, isSupported, _findCameraRollAlbum, };
