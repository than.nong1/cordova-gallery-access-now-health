/**
 * Helper module to ensure that certain permissions are granted by the user.
 * This requires (cordova-plugin-android-permission)[https://github.com/NeoLSN/cordova-plugin-android-permission]
 * to be installed for Android 6 permission checks.
 */
declare type Permission = 'get-album';
/**
 * Ensure that the user granted a specific permission.
 * @param permission Permission that should be granted
 * @return Resolves if the permission was granted. Rejects if the permission was not granted.
 */
export declare function ensurePermission(permission: Permission): Promise<boolean>;
export {};
